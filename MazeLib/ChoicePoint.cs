﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeLib
{
    public class ChoicePoint
    {
        public ChoicePoint(Maze theMaze, Box currentBox)
        {
            theBox = currentBox;
            if (!theBox.hasEastWall)
                consider(theMaze[theBox.Row, theBox.Column + 1]);
            if (!theBox.hasWestWall)
                consider(theMaze[theBox.Row, theBox.Column - 1]);
            if (!theBox.hasSouthWall)
                consider(theMaze[theBox.Row + 1, theBox.Column]);
            if (!theBox.hasNorthWall)
                consider(theMaze[theBox.Row - 1, theBox.Column]);
        }

        private void consider(Box box)
        {
            if (!box.hasBeenVisited)
            {
                thingsToTry.Push(box);
            }
        }


        public Box NextChoice()
        {
            return thingsToTry.Pop();
        }

        public bool HasMoreOptions()
        {
            return thingsToTry.Count != 0;
        }

        Stack<Box> thingsToTry = new Stack<Box>();
        public Box theBox { get; private set; }
    }
}
