﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MazeLib;

namespace MazeForm
{
    public partial class MazeForm : Form
    {
        const int DEFAULT_ROWS = 20;
        const int DEFAULT_COLUMNS = 40;
        const int DEFAULT_SEED = 2345231;

        public MazeForm()
        {
            InitializeComponent();
            ResizeRedraw = true;
            DoubleBuffered = true;
            Paint += MazeForm_Paint;
            solutionTimer = new Timer();
            solutionTimer.Interval = 1;
            solutionTimer.Tick += SolutionTimer_Tick;
            Seed = DEFAULT_SEED;
            Text = String.Format("cs241 Maze Solver - Seed = {0}", Seed);
            theMaze.makeMaze(Seed++);
        }

        private void SolutionTimer_Tick(object sender, EventArgs e)
        {
            if (solution.Count > 0)
            {
                Box box = solution.Pop();
                box.hasBeenVisited = true;
                Invalidate();
            }
            else
            {
                solutionTimer.Stop();
            }
        }

        private void setWorldCoordinates(Graphics graphics)
        {
            graphics.TranslateTransform(0, menuStrip1.Height);

            float height = ClientRectangle.Height - menuStrip1.Height;
            float width = ClientRectangle.Width;
            graphics.ScaleTransform(width / nColumns, height / nRows);
        }

        private void MazeForm_Paint(object sender, PaintEventArgs e)
        {
            setWorldCoordinates(e.Graphics);
            Pen boxPen = new Pen(Brushes.Black, 0);
            foreach (Box box in theMaze)
            {
                if (box.hasNorthWall)
                {
                    e.Graphics.DrawLine(boxPen,
                        new Point(box.Column, box.Row),
                        new Point(box.Column + 1, box.Row));
                }

                if (box.hasSouthWall)
                {
                    e.Graphics.DrawLine(boxPen,
                        new Point(box.Column, box.Row + 1),
                        new Point(box.Column + 1, box.Row + 1));
                }

                if (box.hasEastWall)
                {
                    e.Graphics.DrawLine(boxPen,
                        new Point(box.Column + 1, box.Row),
                        new Point(box.Column + 1, box.Row + 1));
                }

                if (box.hasWestWall)
                {
                    e.Graphics.DrawLine(boxPen,
                        new Point(box.Column, box.Row),
                        new Point(box.Column, box.Row + 1));
                }

                if (box.hasBeenVisited)
                {
                    const float lineWidth = .5f;
                    const float lineMargin = (1.0f - lineWidth) / 2.0f;

                    e.Graphics.FillRectangle(Brushes.Red,
                        new RectangleF(box.Column + lineMargin, box.Row + lineMargin,
                          lineWidth, lineWidth));

                    if (box.Column + 1 < nColumns && !box.hasEastWall &&
                        theMaze[box.Row, box.Column + 1].hasBeenVisited)
                    {
                        e.Graphics.FillRectangle(Brushes.Red,
                            new RectangleF(box.Column + lineMargin, box.Row + lineMargin,
                              1.5f, lineWidth));
                    }

                    if (box.Row + 1 < nRows && !box.hasSouthWall &&
                        theMaze[box.Row + 1, box.Column].hasBeenVisited)
                    {
                        e.Graphics.FillRectangle(Brushes.Red,
                            new RectangleF(box.Column + lineMargin, box.Row + lineMargin,
                              lineWidth, 1.5f));
                    }
                }
            }
        }


        private void setDimensionsToolStripMenuItem_Click(object sender,
            EventArgs e)
        {
            DimensionsForm dialog = new DimensionsForm();
            dialog.Rows.Text = nRows.ToString();
            dialog.Columns.Text = nColumns.ToString();
            dialog.checkBox1.Checked = AnimateSolution;
            dialog.Seed.Text = Seed.ToString();
            DialogResult result = dialog.ShowDialog();
            if (result != DialogResult.Cancel)
            {
                nRows = Int32.Parse(dialog.Rows.Text);
                nColumns = Int32.Parse(dialog.Columns.Text);
                theMaze = new Maze(nRows, nColumns);
                AnimateSolution = dialog.checkBox1.Checked;
                Seed = Int32.Parse(dialog.Seed.Text);
                Invalidate();
            }

        }

        private int nRows = DEFAULT_ROWS;
        private int nColumns = DEFAULT_COLUMNS;
        Maze theMaze = new Maze(DEFAULT_ROWS, DEFAULT_COLUMNS);
        private int Seed = DEFAULT_SEED;
        private bool AnimateSolution = true;

        private void breakWallsToolStripMenuItem_Click(object sender, 
            EventArgs e)
        {
            theMaze = new Maze(nRows, nColumns);
            Text = String.Format("cs241 Maze Solver - Seed = {0}",
                    Seed);
            theMaze.makeMaze(Seed++);
            Invalidate();
        }

        private Stack<Box> solution;    
        private Timer solutionTimer;

        private void displaySolution()
        {
            foreach (Box box in theMaze) box.hasBeenVisited = false;
            solutionTimer.Start();
        }

        private void solveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            solution = theMaze.solve();
            if (AnimateSolution)
            {
                displaySolution();
            }
            else
            {
                while(solution.Count != 0)
                {
                    Box box = solution.Pop();
                    box.hasBeenVisited = true;
                }
                Invalidate();
            }
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DimensionsForm dialog = new DimensionsForm();
            dialog.Rows.Text = nRows.ToString();
            dialog.Columns.Text = nColumns.ToString();
            dialog.checkBox1.Checked = AnimateSolution;
            dialog.Seed.Text = Seed.ToString();
            DialogResult result = dialog.ShowDialog();
            if (result != DialogResult.Cancel)
            {
                nRows = Int32.Parse(dialog.Rows.Text);
                nColumns = Int32.Parse(dialog.Columns.Text);
                theMaze = new Maze(nRows, nColumns);
                AnimateSolution = dialog.checkBox1.Checked;
                Seed = Int32.Parse(dialog.Seed.Text);
                Text = String.Format("cs241 Maze Solver - Seed = {0}",
                    Seed);
                theMaze.makeMaze(Seed++);
                Invalidate();

            }
        }

        private void shortestSolutionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int shortestPathLength;
            int longestPathLength;
            int nSolutions;
            solution = theMaze.findBestSoltuion(out nSolutions,
                out longestPathLength, out shortestPathLength);

            Text = String.Format("nSolutions={0}; longestPath={1}; shortestPath={2}",
                nSolutions, longestPathLength, shortestPathLength);

            if (AnimateSolution)
            {
                displaySolution();
            }
            else
            {
                while(solution.Count != 0)
                {
                    Box box = solution.Pop();
                    box.hasBeenVisited = true;
                }
                Invalidate();
            }
        }

        private void breakWallsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            theMaze.breakRandomWalls(.1f);
            foreach(Box box in theMaze)
            {
                box.hasBeenVisited = false;
            }
            Invalidate();
        }
    }
}
