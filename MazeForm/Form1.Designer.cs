﻿namespace MazeForm
{
    partial class MazeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.breakWallsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.solveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.shortestSolutionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.breakWallsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem,
            this.breakWallsToolStripMenuItem,
            this.solveToolStripMenuItem,
            this.shortestSolutionToolStripMenuItem,
            this.breakWallsToolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.settingsToolStripMenuItem.Text = "Settings";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
            // 
            // breakWallsToolStripMenuItem
            // 
            this.breakWallsToolStripMenuItem.Name = "breakWallsToolStripMenuItem";
            this.breakWallsToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.breakWallsToolStripMenuItem.Text = "NextMaze";
            this.breakWallsToolStripMenuItem.Click += new System.EventHandler(this.breakWallsToolStripMenuItem_Click);
            // 
            // solveToolStripMenuItem
            // 
            this.solveToolStripMenuItem.Name = "solveToolStripMenuItem";
            this.solveToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.solveToolStripMenuItem.Text = "Solve";
            this.solveToolStripMenuItem.Click += new System.EventHandler(this.solveToolStripMenuItem_Click);
            // 
            // shortestSolutionToolStripMenuItem
            // 
            this.shortestSolutionToolStripMenuItem.Name = "shortestSolutionToolStripMenuItem";
            this.shortestSolutionToolStripMenuItem.Size = new System.Drawing.Size(106, 20);
            this.shortestSolutionToolStripMenuItem.Text = "ShortestSolution";
            this.shortestSolutionToolStripMenuItem.Click += new System.EventHandler(this.shortestSolutionToolStripMenuItem_Click);
            // 
            // breakWallsToolStripMenuItem1
            // 
            this.breakWallsToolStripMenuItem1.Name = "breakWallsToolStripMenuItem1";
            this.breakWallsToolStripMenuItem1.Size = new System.Drawing.Size(76, 20);
            this.breakWallsToolStripMenuItem1.Text = "BreakWalls";
            this.breakWallsToolStripMenuItem1.Click += new System.EventHandler(this.breakWallsToolStripMenuItem1_Click);
            // 
            // MazeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MazeForm";
            this.Text = "cs241 Maze Solver";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem breakWallsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem solveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem shortestSolutionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem breakWallsToolStripMenuItem1;
    }
}

