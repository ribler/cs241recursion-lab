﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Collections.Generic;
using RecursionLib;
using MazeLib;

namespace RecursionTest
{
    [TestClass]
    public class UnitTest1
    {

        [TestMethod]
        public void stringWriterTest()
        {
            string thingToWrite = "Hello world!";
            TextWriter writer = new StringWriter();
            writer.WriteLine(thingToWrite);
            writer.Close();

            TextReader reader = new StringReader(writer.ToString());
            Assert.AreEqual(thingToWrite, reader.ReadLine());
            reader.Close();
        }

        [TestMethod]
        public void fileWritetest()
        {
            string thingToWrite = "Hello world!";
            TextWriter writer = new StreamWriter("testFile.txt");
            writer.WriteLine(thingToWrite);
            writer.Close();

            TextReader reader = new StreamReader("testFile.txt");
            Assert.AreEqual(thingToWrite, reader.ReadLine());
            reader.Close();
        }

        [TestMethod]
        public void fileWriteUsingTest()
        {
            string thingToWrite = "Hello world!";
            string result;
            using (TextWriter writer = new StreamWriter("testFile.txt"))
            {
                writer.WriteLine(thingToWrite);
            }

            using (TextReader reader = new StreamReader("testFile.txt"))
            {
                result = reader.ReadLine();
            }
            Assert.AreEqual(thingToWrite, result);
        }

        [TestMethod]
        public void stringWriterUsingTest()
        {
            string thingToWrite = "Hello world!";
            using (TextWriter writer = new StringWriter())
            {
                writer.WriteLine(thingToWrite);
                writer.Close();

                using (TextReader reader = new StringReader(writer.ToString()))
                {
                    Assert.AreEqual(thingToWrite, reader.ReadLine());
                }
            }
        }

        [TestMethod]
        public void printTablsTest()
        {
            string result;
            using (TextWriter writer = new StringWriter())
            {
                NonGenericFunctions.printTabs(writer, 7);
                TextReader reader = new StringReader(writer.ToString());
                result = reader.ReadLine();
            }
            Assert.AreEqual("\t\t\t\t\t\t\t", result);
        }

        [TestMethod]
        public void findFilesInDirectory()
        {
            DirectoryInfo dirInfo = new DirectoryInfo("../../testDir");
            List<string> fileList = new List<string>();
            foreach (FileInfo fileInfo in dirInfo.GetFiles())
            {
                fileList.Add(fileInfo.Name);
            }
            Assert.AreEqual(3, fileList.Count);
            Assert.IsTrue(fileList.Contains("helloCake.sln"));
            Assert.IsTrue(fileList.Contains("gitattributes"));
            Assert.IsTrue(fileList.Contains("gitignore"));
        }

        [TestMethod]
        public void findDirectoriesInDirectory()
        {
            DirectoryInfo rootDir = new DirectoryInfo("../../testDir/helloCake");
            List<string> dirList = new List<string>();
            foreach (DirectoryInfo dirInfo in rootDir.GetDirectories())
            {
                dirList.Add(dirInfo.Name);
            }
            Assert.AreEqual(3, dirList.Count);
            Assert.IsTrue(dirList.Contains("rbin"));
            Assert.IsTrue(dirList.Contains("robj"));
            Assert.IsTrue(dirList.Contains("readme"));
        }

        //[TestMethod]
        //// Add a function called printDirTree to the NonGenericFunctions 
        //// class in RecursionLib that prints the directory names and all 
        //// subdirectory names using an indented format.  In this test, 
        //// the function prints all the directories from the path ../../test.
        //// The results would look something like this:
        ////  test
        ////      test\sub1
        ////          test\sub1\sub11
        ////      test\sub2
        ////
        //// The printDirTree function accepts 4 parameters:
        //// 
        //// The instance of TextWriter that will be used to output the result.
        //// An initial path
        //// The DirectoryInfo for the directory.
        //// A number of tabs to indent.
        ////
        //// The printDirTree function must work recursively to generate
        //// the requested output.
        //public void printDirTreeTest()
        //{
        //    string result;
        //    DirectoryInfo dirInfo = new DirectoryInfo("../../test");
        //    using (TextWriter writer = new StringWriter())
        //    {
        //        NonGenericFunctions.printDirTree(writer, "", dirInfo, 0);
        //        result = writer.ToString();
        //    }
        //    Assert.AreEqual("test\n\ttest\\sub1\n\t\ttest\\sub1\\sub11\n\ttest\\sub2\n",
        //    result);
        //}

        //// Add a function called findAllFiles to the NonGenericFunctions 
        //// findAllFiles adds the filenames of files in the 
        //// specified directory and in all its subdirectories.
        //[TestMethod]
        //public void findAllFilesInDirectory()
        //{
        //    List<string> dirList = new List<string>();
        //    NonGenericFunctions.findAllFiles("../../testDir", dirList);
        //    Assert.AreEqual(30, dirList.Count);
        //}

        ////[TestMethod]
        ////// Implement another version of findAllFiles that returns a List<string>
        ////// containing the filenames.  You can implement this version using
        ////// the other version of findAllFiles.
        ////public void findAllFilesReturnsList()
        ////{

        ////    List<string> dirList = NonGenericFunctions.findAllFiles("../../testDir");
        ////    Assert.AreEqual(30, dirList.Count);
        ////}
        //public void reverseEnumerable()
        //{
        //    // Create things to reverse

        //    // A list of strings
        //    IEnumerable<string> myStrings = new List<string>() { "a", "b", "c" };

        //    // A list of ints
        //    IEnumerable<int> myInts = new List<int>() { 1, 2, 3 };


        //    // Write a Reverse function that takes an IEnumerable as an argument.
        //    // This function must work in a recursive way.
        //    // Hint: The reverse of a list is the reverse of all items
        //    // in the list other than the first item, followed by the first item. 
        //    // Test it with strings and ints.
        //    List<string> revStrings = GenericFunctions<string>.Reverse(myStrings);
        //    List<int> revInt = GenericFunctions<int>.Reverse(myInts);

        //    // Generate the expected results.
        //    List<string> stringResult = new List<string>() { "c", "b", "a" };
        //    List<int> intResult = new List<int>() { 3, 2, 1 };

        //    // Check to see if the functions returned the expected results.
        //    for (int i = 0; i < intResult.Count; i++)
        //    {
        //        Assert.AreEqual(intResult[i], revInt[i]);
        //        Assert.AreEqual(stringResult[i], revStrings[i]);
        //    }
        //}

        //[TestMethod]
        //// Solve the towers of Hanoi problem.
        //// Parameters are:
        ////    TextWriter writer - place to write result.
        ////    Stack<int> source stack (integers represent disk sizes)
        ////    string nameOfSource (one of "left", "right" or "middle").
        ////    Stack<int> temp stack (integers represent disk sizes)
        ////    string nameOfTemp (one of "left", "right" or "middle").
        ////    Stack<int> destination stack (integers represent disk sizes)
        ////    string nameOfDestination (one of "left", "right" or "middle").
        ////    int nDisksToMove - the number of disks to move from source
        ////                       to destination.
        //public void hanoiTest()
        //{
        //    const int nDisks = 4;
        //    Stack<int> source = new Stack<int>();
        //    Stack<int> middle = new Stack<int>();
        //    Stack<int> destination = new Stack<int>();

        //    for (int i = nDisks; i > 0; i--)
        //    {
        //        source.Push(i);
        //    }

        //    List<string> result = new List<string>();

        //    using (TextWriter writer = new StringWriter())
        //    {

        //        // write a line
        //        // like "Move disk 1 from left pole to middle pole"
        //        NonGenericFunctions.hanoi(writer, source, "left", middle, "middle",
        //            destination, "right", source.Count);

        //        using (StringReader reader = new StringReader(writer.ToString()))
        //        {
        //            while (reader.Peek() != -1)
        //            {
        //                result.Add(reader.ReadLine());
        //            }
        //        }
        //    }

        //    Assert.IsTrue(source.Count == 0);
        //    Assert.IsTrue(middle.Count == 0);

        //    for (int i = 1; i <= nDisks; i++)
        //    {
        //        Assert.AreEqual(i, destination.Pop());
        //    }

        //    Assert.AreEqual(Math.Pow(2, nDisks) - 1, result.Count);
        //}

        //[TestMethod]
        //// Solve a simple maze.  The result should be generated using
        //// a stack of ChoicePoints as was discussed in class.
        //public void solveSimpleMaze()
        //{
        //    int nRows = 3;
        //    int nColumns = 3;
        //    Maze myMaze = new Maze(nRows, nColumns);

        //    myMaze.RemoveWallsBetween(myMaze[0, 0], myMaze[0, 1]);
        //    myMaze.RemoveWallsBetween(myMaze[0, 0], myMaze[1, 0]);
        //    myMaze.RemoveWallsBetween(myMaze[1, 0], myMaze[2, 0]);
        //    myMaze.RemoveWallsBetween(myMaze[0, 1], myMaze[1, 1]);
        //    myMaze.RemoveWallsBetween(myMaze[1, 1], myMaze[1, 2]);
        //    myMaze.RemoveWallsBetween(myMaze[1, 2], myMaze[2, 2]);

        //    Stack<Box> solution = myMaze.solve();
        //    Assert.AreEqual(5, solution.Count);
        //    Assert.IsTrue(new Box(0, 0).Equals(solution.Pop()));
        //    Assert.IsTrue(new Box(0, 1).Equals(solution.Pop()));
        //    Assert.IsTrue(new Box(1, 1).Equals(solution.Pop()));
        //    Assert.IsTrue(new Box(1, 2).Equals(solution.Pop()));
        //    Assert.IsTrue(new Box(2, 2).Equals(solution.Pop()));
        //}

        //[TestMethod]
        //// This is an additional test of the maze solver.  You shouldn't
        //// need to write any additional code to get this test to run.
        //public void solveSimpleMazeAlternate()
        //{
        //    int nRows = 3;
        //    int nColumns = 3;
        //    Maze myMaze = new Maze(nRows, nColumns);

        //    myMaze.RemoveWallsBetween(myMaze[0, 0], myMaze[0, 1]);
        //    myMaze.RemoveWallsBetween(myMaze[0, 0], myMaze[1, 0]);
        //    myMaze.RemoveWallsBetween(myMaze[1, 0], myMaze[2, 0]);
        //    myMaze.RemoveWallsBetween(myMaze[0, 1], myMaze[1, 1]);
        //    myMaze.RemoveWallsBetween(myMaze[1, 1], myMaze[1, 2]);
        //    myMaze.RemoveWallsBetween(myMaze[2, 0], myMaze[2, 1]);
        //    myMaze.RemoveWallsBetween(myMaze[2, 1], myMaze[2, 2]);

        //    Stack<Box> solution = myMaze.solve();
        //    Assert.AreEqual(5, solution.Count);
        //    Assert.IsTrue(new Box(0, 0).Equals(solution.Pop()));
        //    Assert.IsTrue(new Box(1, 0).Equals(solution.Pop()));
        //    Assert.IsTrue(new Box(2, 0).Equals(solution.Pop()));
        //    Assert.IsTrue(new Box(2, 1).Equals(solution.Pop()));
        //    Assert.IsTrue(new Box(2, 2).Equals(solution.Pop()));
        //}

        //[TestMethod]
        //// Add a member function to the Maze class called findBestSolution
        //// This function will find all solutions to the maze and return
        //// a stack containing the best solution.
        //// Parameters to the function are:
        ////   int nSolutions - an output parameter that will store the total
        ////                number of solutions.
        ////   int longestPathLength - an output parameter that will store the
        ////                           length of the longest path.
        ////   int shortestPathLength - an output parameter that will store the
        ////                           length of the shortest path.
        ////
        //public void findBestSolution()
        //{
        //    int nRows = 3;
        //    int nColumns = 3;
        //    Maze myMaze = new Maze(nRows, nColumns);
        //    int seed = 1234567;
        //    myMaze.makeMaze(seed);
        //    int nSolutions;
        //    int longestPathLength;
        //    int shortestPathLength;

        //    Stack<Box> bestSolution = myMaze.findBestSoltuion(out nSolutions,
        //        out longestPathLength, out shortestPathLength);

        //    Assert.AreEqual(1, nSolutions);
        //    Assert.AreEqual(7, shortestPathLength);
        //    Assert.AreEqual(7, longestPathLength);
        //}

        //// A larger test for findBestSolution.
        //[TestMethod]
        //public void findBestSolutionInBiggerMaze()
        //{
        //    int nRows = 10;
        //    int nColumns = 10;
        //    Maze myMaze = new Maze(nRows, nColumns);
        //    int seed = 1234567;
        //    myMaze.makeMaze(seed);

        //    Stack<Box> bestSolution = myMaze.findBestSoltuion(out int nSolutions,
        //        out int longestPathLength, out int shortestPathLength);

        //    Assert.AreEqual(1, nSolutions);
        //    Assert.AreEqual(33, shortestPathLength);
        //    Assert.AreEqual(33, longestPathLength);
        //}

        // A still larger test for findBestSolution.
        //[TestMethod]
        //public void findBestSolutionWithWallsBroken()
        //{
        //    int nRows = 10;
        //    int nColumns = 10;
        //    Maze myMaze = new Maze(nRows, nColumns);
        //    int seed = 1234567;
        //    myMaze.makeMaze(seed);
        //    float percentageToBreak = .3f;
        //    myMaze.breakRandomWalls(percentageToBreak);

        //    Stack<Box> bestSolution = myMaze.findBestSoltuion(out int nSolutions,
        //        out int longestPathLength, out int shortestPathLength);

        //    Assert.AreEqual(316, nSolutions);
        //    Assert.AreEqual(27, shortestPathLength);
        //    Assert.AreEqual(71, longestPathLength);
        //}
    }
}
