﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace RecursionLib
{
    public class NonGenericFunctions
    {
        public static void printTabs(TextWriter writer, int nTabsToWrite)
        {
            if (nTabsToWrite != 0)
            {
                writer.Write("\t");
                printTabs(writer, nTabsToWrite - 1);
            }
        }
    }
}
